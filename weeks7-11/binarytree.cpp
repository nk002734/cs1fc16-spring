// BinaryTree.cpp :
// This has functions to create and print a binary sorted tree of names.
// Prof Richard Mitchell  20/01/22
//

#include "cardlib.h"
#include <string>
#include <iostream>
using namespace std;
const int maxCard = 10;
aCard thePack[maxCard];

// structure for a node in a binary sorted tree
struct treeNode
{
    aCard data; // actual data
    treeNode *less, *more, *treeTop;
    // pointer to top of tree
    // pointers to node with data less
    // or more than data in this node
};

treeNode *treeTop;        // pointer to top of tree
string cardToStr(aCard c) // takes a card and returns a string
{
    string csuit = "HCDS";
    return csuit.substr(c.cardSuit, 1) + to_string(c.cardVal);
    // c.cardSuit represents the suit, stored as an integer. If the cardSuit is 1 then the first letter of csuit is used.
    // to_string(c.cardSuit) gives the cards number and is concatenated to the string.
}

treeNode *newNode(aCard c)
{
    // create a new node with data s,
    // return pointer to it
    treeNode *p = new treeNode;
    // create space for node
    p->data = c;    // add data
    p->less = NULL; // pointers less and more
    p->more = NULL; // are set to NULL
    return p;       // return pointer to new node
}
int compareCards(aCard c1, aCard c2) // function to compare cards.
{
    int comp;
    // H<C<D<S
    if (c1.cardSuit > c2.cardSuit)
    {
        comp = 1; // c1 bigger than c2
    }
    else if (c1.cardSuit < c2.cardSuit)
    {
        comp = -1; // c1 smaller than c2
    }
    else if (c1.cardSuit == c2.cardSuit) // c1 and c2 are equal
    {
        if (c1.cardVal > c2.cardVal)
        {
            comp = 1; // c1 bigger than c2
        }
        else if (c1.cardVal < c2.cardVal)
        {
            comp = -1; // c1 smaller than c2
        }
        else if (c1.cardVal == c2.cardVal)
        {
            comp = 0; // c1 equals c2
        }
    }
    // cout << cardToStr(c1) << " " << cardToStr(c2) << endl;
    // return -1 (c1 < c2), 0 (c1 == c2), 1 (c1 > c2)
    return comp;
}
void printTree(treeNode *p)
{
    // print the tree from node p
    if (p != NULL)
    {
        printTree(p->less);
        // print any nodes in less sub tree
        cout << cardToStr(p->data) << ", "; // print node
        printTree(p->more);
        // print any nodes in more sub tree
    }
}

treeNode *insertTree(treeNode *p, aCard c)
{
    treeNode *ans = p;
    if (p == NULL)
        ans = newNode(c);                  // data entered into this node
    else if (compareCards(p->data, c) > 0) // checks if card is less than the previous one
        p->less = insertTree(p->less, c);
    else if (compareCards(p->data, c) <= 0) // checks if card is greater than or equal to the previous one
        p->more = insertTree(p->more, c);
    return ans;
    // return pointer to new node, or this
}

int main()
{
    cout << "RJM's Tree Program!\n";
    treeTop = NULL; // initially an empty tree
    for (int ct = 0; ct < maxCard; ct++)
    {
        thePack[ct] = getCard("30002734");          // generates the pack of random cards
        treeTop = insertTree(treeTop, thePack[ct]); // generates the tree with random cards
    }
    printTree(treeTop); // and print
    cout << "\n";
}