int partition(aCard thePack[], int start, int end)
{
    cout << "d" << endl; 
    aCard pivot = thePack[start];

    int count = 0;
    for (int i = start + 1; i <= end; i++)
    {
        if (compareCards(thePack[i], pivot) == -1 || compareCards(thePack[i], pivot) == 0)
            count++;
    }

    // Giving pivot element its correct position
    int pivotIndex = start + count;
    swapCards(pivotIndex, start);

    // Sorting left and right parts of the pivot element
    int i = start, j = end;

    while (i < pivotIndex && j > pivotIndex)
    {
        while (compareCards(thePack[i], pivot) == -1 || compareCards(thePack[i], pivot) == 0)
        {
            i++;
        }

        while (compareCards(thePack[i], pivot) == 1)
        {
            j--;
        }

        if (i < pivotIndex && j > pivotIndex)
        {
            swapCards(i++, j--);
        }
    }

    return pivotIndex;
}
void quickSort(aCard thePack[], int start, int end)
{

    // base case
    if (start >= end)
    {
        return;
    cout << "1 " << start << " " << end << endl;

    }
    cout << "a " << start << " " << end << endl;
    // partitioning the array
    int p = partition(thePack, start, end);

    // Sorting the left part
    quickSort(thePack, start, p - 1);
    cout << "b" << endl;

    // Sorting the right part
    quickSort(thePack, p + 1, end);
    cout << "c" << endl;
}