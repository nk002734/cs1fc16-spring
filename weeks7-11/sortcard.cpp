// SortCard.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "cardlib.h"

#include <string>
#include <iostream>
using namespace std;

const int maxCard = 20;
aCard thePack[maxCard];

string cardToStr(aCard c) // takes a card and returns a string
{
    string csuit = "HCDS";
    return csuit.substr(c.cardSuit, 1) + to_string(c.cardVal);
    // c.cardSuit represents the suit, stored as an integer. If the cardSuit is 1 then the first letter of csuit is used.
    // to_string(c.cardSuit) gives the cards number and is concatenated to the string.
}
void printPack(string mess)
{
    cout << mess
         << ":"
         << "\n";                        // prints message
    for (int ct = 0; ct < maxCard; ct++) // loops through the pack of cards
    {
        cout << cardToStr(thePack[ct]) << ", "; // prints each card seperated by a comma
    }
    cout << "\n";
}
int compareCards(aCard c1, aCard c2) // function to compare cards.
{
    int comp;
    // H<C<D<S
    if (c1.cardSuit > c2.cardSuit)
    {
        comp = 1; // c1 bigger than c2
    }
    else if (c1.cardSuit < c2.cardSuit)
    {
        comp = -1; // c1 smaller than c2
    }
    else if (c1.cardSuit == c2.cardSuit) // c1 and c2 are equal
    {
        if (c1.cardVal > c2.cardVal)
        {
            comp = 1; // c1 bigger than c2
        }
        else if (c1.cardVal < c2.cardVal)
        {
            comp = -1; // c1 smaller than c2
        }
        else if (c1.cardVal == c2.cardVal)
        {
            comp = 0; // c1 equals c2
        }
    }
    // cout << cardToStr(c1) << " " << cardToStr(c2) << endl;
    // return -1 (c1 < c2), 0 (c1 == c2), 1 (c1 > c2)
    return comp;
}
void swapCards(int n1, int n2) // function to swap cards
{
    aCard temp; // temporary variable
    temp = thePack[n1];
    thePack[n1] = thePack[n2];
    thePack[n2] = temp; // n1 and n2 are swapped
}
void bubbleSort(aCard thePack[]) // bubbleSort function
{
    int moves, compares; // variables to count the amount of moves and comparisons
    moves = 0;
    compares = 0;
    for (int i = 0; i < maxCard; i++) // first loop goes through the whole pack
    {
        for (int j = 0; j < maxCard - i - 1; j++) // each time the pack is looped through, 1 card will have bubbled to the top so the loop has to start again but 1 less time.
        {
            compares++;                                        // incrementing comparisons
            if (compareCards(thePack[j], thePack[j + 1]) == 1) // compares two cards next to eachother
            {
                moves++;
                swapCards(j, j + 1); // if the first card is larger, then the cards will be swapped.
            }
        }
    }
    cout << "Bubble sort Moves: " << moves << " Compares: " << compares << endl; // outputting the number of moves and comparisons.
}
int partition(aCard thePack[], int start, int end)
{
    cout << "d" << endl;
    aCard pivot = thePack[start];

    int count = 0;
    for (int i = start + 1; i <= end; i++)
    {
        if (compareCards(thePack[i], pivot) == -1 || compareCards(thePack[i], pivot) == 0)
            count++;
    }

    // Giving pivot element its correct position
    int pivotIndex = start + count;
    swapCards(pivotIndex, start);

    // Sorting left and right parts of the pivot element
    int i = start, j = end;

    while (i < pivotIndex && j > pivotIndex)
    {
        while (compareCards(thePack[i], pivot) == -1 || compareCards(thePack[i], pivot) == 0)
        {
            i++;
        }

        while (compareCards(thePack[i], pivot) == 1)
        {
            j--;
        }

        if (i < pivotIndex && j > pivotIndex)
        {
            swapCards(i++, j--);
        }
    }

    return pivotIndex;
}
void quickSort(aCard thePack[], int start, int end)
{

    // base case
    if (start >= end)
    {
        return;
        cout << "1 " << start << " " << end << endl;
    }
    cout << "a " << start << " " << end << endl;
    // partitioning the array
    int p = partition(thePack, start, end);

    // Sorting the left part
    quickSort(thePack, start, p - 1);
    cout << "b" << endl;

    // Sorting the right part
    quickSort(thePack, p + 1, end);
    cout << "c" << endl;
}
void qSort(aCard thePack[], int lndx, int hndx)
{
    int i, j;
    aCard pivot = thePack[lndx];
    // sort in array between indices
    // Calculate pivot
    // in the middle
    i = lndx;
    j = hndx;
    while (i < j)
    {
        while (compareCards(pivot, thePack[i]) == 1)
        {
            i++;
            // find element > pivot to swap
        }

        while (compareCards(pivot, thePack[j]) == -1)
        {
            j--;
            // find element < pivot to swap
        }

        if (i <= j)
        {
            if (i < j)
            {
                swapCards(i, j);
                i++;
                j--;
            }
        }
    }
    if (lndx < j)
        qSort(thePack, lndx, j);
    if (i < hndx)
        qSort(thePack, i, hndx);
}

int main()
{
    cout << "Card Sorting!\n";
    for (int ct = 0; ct < maxCard; ct++) // for loop for each card
    {
        thePack[ct] = getCard("30002734"); // generate random card
    }
    printPack("Unsorted"); // prints unsorted pack
    qSort(thePack, 0, 10);
    // bubbleSort(thePack); // calls bubblesort function
    printPack("Sorted"); // prints sorted pack
}