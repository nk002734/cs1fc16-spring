// Factorial.cpp : This file contains the 'main' function. Program execution begins and ends there.
// Written by Prof Richard Mitchell    18/1/22
////

#include <iostream>
using namespace std;

int factorial(int n)
{
    // returns the factorial of integer n which is >= 0
    // factorial of 0 or 1 is 1, otherwise it is n times factorial of n-1
    if (n > 1)
        return n * factorial(n - 1);
    else
        return 1;
}

int main()
{
    cout << "Factorial Example\nWritten by Richard Mitchell\n";
    // program calculates and outputs the factorial of 0 to 10
    for (int ct = 0; ct <= 10; ct++)
        cout << "Factorial " << ct << " is " << factorial(ct) << "\n";
}
