#include <iostream>
using namespace std;
int main()
{
    int a, b, c; //define 3 variables to store previous, current and next values for fib sequence
    b = 0; //b starts at 0
    c = 1; //c starts at 1 to start the sequence
    for (int i = 0; i <= 30; i++) //iterative loop from 0 to 30
    {
        a = b; //previous number = current number
        b = c; //current number = next number
        c = a + b; //new next number is found by adding the 2 previous highest numbers
        cout << "fib " << i << " is " << a << "\n"; //count and fib value is outputted
    }
}