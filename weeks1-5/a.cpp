// MyMatLb.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <string>
#include <iostream>
using namespace std;

// matlib.cpp : A simple matrix library program.
// Written by Prof Richard Mitchell    7/1/22
//

struct myMat
{                // allows for matrices up to size 4*4
    int numRows; // number of rows
    int numCols;
    int data[16]; // data are stored in row order
};

myMat zeroMat(int r, int c)
{
    // create a matrix with r rows and c columns, filled with zeros
    myMat m;       // define matrix
    m.numRows = r; // set size
    m.numCols = c;
    for (int ct = 0; ct < 16; ct++)
        m.data[ct] = 0; // set elems to 0
    return m;           // return matrix
}

int getElem(myMat m, int r, int c)
{
    // return the item at m[r,c]   where r is 0..m.numRows-1 etc
    return m.data[r * m.numCols + c];
}

void setElem(myMat &m, int r, int c, int val)
{
    // set element m[r,c] to val
    m.data[r * m.numCols + c] = val;
}

myMat mFromStr(string s)
{
    // create a matrix from string s
    // string of form "1,2;3,4;5,6"   ; separates rows and , separates columns ... No error check
    int ms;
    if (s.length() > 0)
        ms = 1;
    else
        ms = 0;
    myMat m = zeroMat(ms, ms); // is s empty create 0*0 matrix, else start with 1*1 matrix
    int mndx = 0;              // used to index into array
    string sub = "";           // sub string - numbers between , or ; set empty
    for (int ct = 0; ct < s.length(); ct++)
    { // each char in turn
        if ((s[ct] == ',') || (s[ct] == ';'))
        {                               // if , or ; then sub contains number
            m.data[mndx++] = stoi(sub); // convert number to integer, put in data array
            sub = "";                   // clear sub string
            if (s[ct] == ';')
                m.numRows++; // if found ; indicates an extra row
            else if (m.numRows == 1)
                m.numCols++; // if , then (if in row 1) increase count of columns
        }
        else
            sub = sub + s[ct]; // add character to sub string
    }
    if (sub.length() > 0)
        m.data[mndx++] = stoi(sub); // add last sub string
    return m;
}

myMat mGetRow(myMat m, int row)
{
    // create a matrix from m, having one row
    myMat res = zeroMat(1, m.numCols);        // create a matrix of 1 row
    for (int col = 0; col < m.numCols; col++) // for each element in row
        res.data[col] = getElem(m, row, col); // copy col element to res
    return res;
}

/*
myMat mGetCol(myMat m, int col) {
// create a matrix from m, having one col
}

int dotProd(myMat v1, myMat v2) {
}

myMat mTranspose(myMat m) {
}

myMat mAdd(myMat m1, myMat m2) {
}

myMat mMult(myMat m1, myMat m2) {
}
*/

void printMat(const char *mess, myMat m)
{
    // mess is string to be printed, followed by matrix m
    cout << mess << " = "
         << "\n"; // print message
    for (int r = 0; r < m.numRows; r++)
    {                                         // do each row
        for (int c = 0; c < m.numCols; c++)   // do each column
            cout << getElem(m, r, c) << "\t"; // outputing the element then tab
        cout << "\n";                         // output new line at end of row
    }
    cout << "\n"; // and end of Matrix
}

int main()
{
    cout << "Ric's Matrix Example Program\n";
    // myMat m1, m2, m3, m4, m5;

    // m1 = mFromStr("1,2,3;4,-5,6");
    // m2 = mFromStr("2,-5;3,7;1,4");
    // printMat("m1", m1);                   // display m1
    // printMat("m2", m2);                   // display m2
    // printMat("m1 row 0", mGetRow(m1, 0)); // display row 0 of m1
    // m3 = mAdd(m1, mTranspose(m2));
    // printMat("m1+m2^T", m3);
    // m4 = mMult(m1, m2);
    // printMat("m1*m2", m4);
    // m5 = mMult(m2, m1);
    // printMat("m2*m1", m5);

    return 0;
}
