#include <iostream>
using namespace std;
int fib(int n) // fib function
{
    if (n > 1)                          // fib of 1 returns 1, otherwise the fib algorithm is executed
        return fib(n - 1) + fib(n - 2); // the previous number plus the one before it
    else
        return 1; // return 1 when n is less than 1
}
int main()
{
    for (int i = 0; i <= 29; i++)                            // for loop to do fib function on numbers 0 to 30
        cout << "fib " << i + 1 << " is " << fib(i) << "\n"; // console output of fib value
}